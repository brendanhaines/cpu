# basic_test

Verify basic usage of all instructions. Includes tests to ensure pipeline stalls sufficiently for correctness but does not test for unnecessary stalls.

Currently store/load does not implement proper stalling so these operations are padded with nop