![pipeline status](https://gitlab.com/brendanhaines/0039_cpu/badges/master/pipeline.svg)

# RISC-V CPU

Short Term To Do:
* add stalls for memory access
* use AXI for memory access (depends on AXIL memory module for test)
* add tests for non-pipelined case
* get C working (may depend on memory stalls)

Desired features:
* 1- or 5-stage pipeline selectable via parameter
* AXI-lite Master for both instruction and data memory
* 32, 64, (or 128?) bit word size
* floating point
* multiplication
* division
* instruction and data caches
* JTAG debug probe

## Installation
Run `setup.sh` to install GCC

## Resources
* [AXI4 Protocol Specification](https://developer.arm.com/documentation/ihi0022/e/AMBA-AXI3-and-AXI4-Protocol-Specification?lang=en)
