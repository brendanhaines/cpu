interface axi_lite();
    parameter DATA_WIDTH = 32;
    parameter ADDR_WIDTH = 12;
    // Global
    logic                           ACLK;
    logic                           ARESETn;

    // Write address
    logic                           AWVALID;
    logic   [ADDR_WIDTH-1:0]        AWADDR;
    logic   [2:0]                   AWPROT;
    logic                           AWREADY;

    // Write data
    logic                           WVALID;
    logic   [DATA_WIDTH-1:0]        WDATA;
    logic   [(DATA_WIDTH/8)-1:0]    WSTRB;
    logic                           WREADY;

    // Write response
    logic                           BVALID;
    logic                           BREADY;
    logic   [1:0]                   BRESP;

    // Read address
    logic                           ARVALID;
    logic   [ADDR_WIDTH-1:0]        ARADDR;
    logic   [2:0]                   ARPROT;
    logic                           ARREADY;

    // Read data
    logic                           RVALID;
    logic   [DATA_WIDTH-1:0]        RDATA;
    logic   [1:0]                   RRESP;
    logic                           RREADY;
endinterface
