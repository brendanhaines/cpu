interface test_if();

parameter int DW = 32;

logic[DW-1:0] data;

modport consumer (
    input data
);

endinterface

module test_mod(
    test_if.consumer if_in,
    output[31:0] dout
);

assign dout = if_in.data;


endmodule