// `include "axi_lite_if.sv"
interface axi_lite_if();
    parameter DATA_WIDTH = 32;
    parameter ADDR_WIDTH = 12;

    logic RREADY;

    modport master (
        output RREADY
    );

    modport slave (
    // // Global
    // input                               ACLK,
    // input                               ARESETn,

    // // Write address
    // input                           AWVALID,
    // // input   [ADDR_WIDTH-1:0]        AWADDR,
    // // input   [2:0]                   AWPROT,
    // output                          AWREADY,

    // // Write data
    // input                           WVALID,
    // // input   [DATA_WIDTH-1:0]        WDATA,
    // // input   [(DATA_WIDTH/8)-1:0]    WSTRB,
    // output                          WREADY,

    // // Write response
    // output                          BVALID,
    // input                           BREADY,
    // // output  [1:0]                   BRESP,

    // // Read address
    // input                           ARVALID,
    // // input   [ADDR_WIDTH-1:0]        ARADDR,
    // // input   [2:0]                   ARPROT,
    // output                          ARREADY,

    // // Read data
    // output                          RVALID,
    // output  [DATA_WIDTH-1:0]        RDATA,
    // output  [1:0]                   RRESP,
    input                           RREADY
    );
endinterface


module test_sv(
    axi_lite_if.slave s_axil,
    input clk,
    output c, d

);

logic a, b;

assign a = clk;
always @(*) begin
    b = !clk;
end

assign c = a;
assign d = b;

endmodule