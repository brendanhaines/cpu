```wavedrom
{
    signal: [
    {name: 'ACLK',          wave: 'P..|..'},
    ['Write Address',
        {name: 'AWVALID',   wave: '010|..', data: []},
        {name: 'AWADDR',    wave: 'x3x|..', data: [1,2,3,4]},
        {name: 'AWPROT',    wave: 'x3x|..', data: [1,2,3,4]},
        {name: 'AWREADY',   wave: '1..|..', data: []},
    ],
    ['Write Data',
        {name: 'WVALID',    wave: '010|..', data: []},
        {name: 'WDATA',     wave: 'x3x|..', data: [1,2,3,4]},
        {name: 'WSTRB',     wave: 'x3x|..', data: [1,2,3,4]},
        {name: 'WREADY',    wave: '1..|..', data: []},
    ],
    ['Write Resp',
        {name: 'BVALID',    wave: '0..|10', data: []},
        {name: 'BREADY',    wave: '01.|.0', data: []},
        {name: 'BRESP',     wave: 'x..|3x', data: [1,2,3,4]},
    ],
    ],
    head:{
        text:'AXI-Lite Write Example',
        tick:0,
    },
    foot:{
        text:'Slave may take arbitrarily long to respond',
    }
}

```
```wavedrom
{
    signal: [
    {name: 'ACLK',          wave: 'P.......'},
    ['Write Address',
        {name: 'AWVALID',   wave: '01...0..', data: []},
        {name: 'AWADDR',    wave: 'x345.x..', data: [1,2,3,4]},
        {name: 'AWPROT',    wave: 'x345.x..', data: [1,2,3,4]},
        {name: 'AWREADY',   wave: '1..01...', data: []},
    ],
    ['Write Data',
        {name: 'WVALID',    wave: '010.1.0.', data: []},
        {name: 'WDATA',     wave: 'x3x.45x.', data: [1,2,3,4]},
        {name: 'WSTRB',     wave: 'x3x.45x.', data: [1,2,3,4]},
        {name: 'WREADY',    wave: '1.......', data: []},
    ],
    ['Write Resp',
        {name: 'BVALID',    wave: '0.10.1.0', data: []},
        {name: 'BREADY',    wave: '01.....0', data: []},
        {name: 'BRESP',     wave: 'x.3x.45x', data: [1,2,3,4]},
    ],
    ],
    head:{
        text:'AXI-Lite Write Example',
        tick:0,
    }
}

```

```wavedrom
{
    signal: [
    {name: 'ACLK',          wave: 'P.....'},
    ['Write Address',
        {name: 'AWVALID',   wave: '01..0.', data: []},
        {name: 'AWADDR',    wave: 'x345x.', data: [1,2,3,4]},
        {name: 'AWPROT',    wave: 'x345x.', data: [1,2,3,4]},
        {name: 'AWREADY',   wave: '1...0.', data: []},
    ],
    ['Write Data',
        {name: 'WVALID',    wave: '01..0.', data: []},
        {name: 'WDATA',     wave: 'x345x.', data: [1,2,3,4]},
        {name: 'WSTRB',     wave: 'x345x.', data: [1,2,3,4]},
        {name: 'WREADY',    wave: '1.....', data: []},
    ],
    ['Write Resp',
        {name: 'BVALID',    wave: '0.1..0', data: []},
        {name: 'BREADY',    wave: '01...0', data: []},
        {name: 'BRESP',     wave: 'x.345x', data: [1,2,3,4]},
    ],
    ],
    head:{
        text:'AXI-Lite Write Example',
        tick:0,
    }
}

```