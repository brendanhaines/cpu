module correlator #(
    parameter LENGTH = 8,
    parameter BITS_IN = 8,
    parameter BITS_INTERNAL = BITS_IN + $clog2(LENGTH),
    parameter BITS_OUT = 8,
)(
    input  wire clk,
    input  wire reset,

    input  wire [BITS_IN-1:0]  a,
    input  wire [BITS_IN-1:0]  b,
    output reg  [BITS_OUT-1:0] y
);

// verify parameters are valid
if (BITS_OUT > BITS_INTERNAL) begin
    $error("BITS_OUT (%d) must be <= BITS_INTERNAL (%d)", BITS_OUT, BITS_INTERNAL);
end

// signals
reg [BITS_IN-1] aa [0:LENGTH-1];
reg [BITS_IN-1] bb [0:LENGTH-1];
reg [BITS_INTERNAL-1:0] sum;

// combinatorial calculation
always @(*) begin : continuous
    integer i;

    aa[0] = a;
    bb[0] = b;

    sum = 0;
    for (i=0; i<LENGTH; i=i+1) begin
        sum = sum + aa[i] * bb[i]
    end
end

// synchronous update
always @(posedge clk or posedge reset) begin : update
    integer i;
    if (reset) begin
        for (i=1; i<LENGTH; i=i+1) begin
            aa[i] <= 0;
            bb[i] <= 0;
        end
        y <= 0;
    end else begin        
        for (i=1; i<LENGTH; i=i+1) begin
            aa[i] <= aa[i-1];
            bb[i] <= bb[i-1];
        end
        y <= sum[BITS_OUT-1:0];
    end
end

endmodule