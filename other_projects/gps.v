module gps #(
    parameter BITS_IN,
)(
    input  wire clk,
    input  wire reset,

    input [BITS_IN-1:0] in_i,
    input [BITS_IN-1:0] in_q,

);

for (prn=1; prn<=32; prn=prn+1) begin
    correlator #(
        .BITS_IN(BITS_IN),
    ) cor(
        .clk(clk),
        .reset(reset),
        .a()
    );
end

endmodule