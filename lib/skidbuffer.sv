module skidbuffer #(
    parameter WIDTH = 1
)(
    input logic clk,
    input logic reset,

    input logic [WIDTH-1:0] in,
    input logic in_valid,
    output logic in_ready,

    output logic [WIDTH-1:0] out,
    output logic out_valid,
    input logic out_ready
);
    logic buffer_filled = 0;
    logic [WIDTH-1:0] buffer_val;

    always_ff @(posedge clk) begin
        if (reset) begin
            buffer_filled <= 0;
        end else begin
            if (in_valid && in_ready) begin
                // input always gets stored whether it needs to be or not
                buffer_val <= in;
            end

            if (buffer_filled) begin
                if ((out_valid && out_ready) && !(in_valid && in_ready)) begin
                    // out_valid = 1 since buffer is full
                    buffer_filled <= 0;
                end
            end else begin
                if ((in_valid && in_ready) && !(out_valid && out_ready)) begin
                    // in_ready = 1 since buffer is empty
                    buffer_filled <= 1;
                end
            end
        end
    end

    always_comb begin
        if (buffer_filled) begin
            in_ready = out_ready;
            out_valid = 1;
            out = buffer_val;
        end else begin
            in_ready = 1;
            out_valid = in_valid;
            out = in;
        end
    end

endmodule