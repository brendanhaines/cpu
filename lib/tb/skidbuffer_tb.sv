`include "bh_assert.sv"
`timescale 1ns/1ps

import bh_assert::bh_assert_equal;
import bh_assert::bh_assert_stats;

module skidbuffer_tb();
    parameter WIDTH = 15;
    parameter TEST_LIST_LENGTH = 256;

    logic clk = 0;
    logic reset = 1;
    logic [WIDTH-1:0] in;
    logic in_valid = 0;
    wire in_ready;
    wire [WIDTH-1:0] out;
    wire out_valid;
    logic out_ready = 0;

    skidbuffer #(
        .WIDTH(WIDTH)
    ) dut (
        .clk(clk),
        .reset(reset),
        .in(in),
        .in_valid(in_valid),
        .in_ready(in_ready),
        .out(out),
        .out_valid(out_valid),
        .out_ready(out_ready)
    );

    integer i = 0;
    integer in_count = 0;
    integer out_count = 0;
    logic [WIDTH-1:0] in_list [0:TEST_LIST_LENGTH-1];

    assign in = in_list[in_count];

    always #5 clk = !clk;

    initial begin
        $dumpfile("skidbuffer_tb.vcd");
        $dumpvars(0, skidbuffer_tb);

        for (i=0; i<TEST_LIST_LENGTH; i=i+1) begin
            in_list[i] = $urandom();
        end

        #10
        reset = 0;

        while (out_count < TEST_LIST_LENGTH) begin
            #10
            if (!in_valid || (in_valid && in_ready)) begin
                in_valid = $urandom_range(1);
            end
            if (!out_ready || (out_ready && out_valid)) begin
                out_ready = $urandom_range(1);
            end
        end

        #10
        bh_assert_stats();
        $finish;
    end

    always @(posedge clk) begin
        if (reset == 0 && in_valid && in_ready) begin
            in_count <= in_count + 1;
        end

        if (reset == 0 && out_valid && out_ready) begin
            bh_assert_equal(out, in_list[out_count], $sformatf("Output value [%3d]", out_count));
            out_count <= out_count + 1;
        end
    end

    wire [WIDTH-1:0] out_correct;
    assign out_correct = in_list[out_count];

endmodule
