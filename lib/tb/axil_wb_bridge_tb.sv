`include "bh_assert.sv"
`timescale 1ns/1ps

import bh_assert::bh_assert_equal;
import bh_assert::bh_assert_stats;
import bh_assert::bh_info;

module axil_wb_bridge_tb();

parameter ADDR_WIDTH = 8;
parameter DATA_WIDTH = 32;

logic clk = 0;
logic reset = 1;

logic axil_awvalid = 0;
wire axil_awready;
logic [ADDR_WIDTH-1:0] axil_awaddr;
logic [2:0] axil_awprot;

logic axil_wvalid = 0;
wire axil_wready;
logic [DATA_WIDTH-1:0] axil_wdata;
logic [DATA_WIDTH/8 - 1:0] axil_wstrb;

wire axil_bvalid;
logic axil_bready = 0;
wire [1:0] axil_bresp;

logic axil_arvalid = 0;
wire axil_arready;
logic [ADDR_WIDTH-1:0] axil_araddr;
logic [2:0] axil_arprot;

wire axil_rvalid;
logic axil_rready = 0;
wire [DATA_WIDTH-1:0] axil_rdata;
wire [2:0] axil_rresp;

wire [ADDR_WIDTH-1:0] wb_adr_o;
logic [DATA_WIDTH-1:0] wb_dat_i;
wire [DATA_WIDTH-1:0] wb_dat_o;
wire wb_we_o;
wire wb_sel_o;
wire wb_stb_o;
logic wb_ack_i = 0;
wire wb_cyc_o;

axil_wb_bridge #(
    .ADDR_WIDTH(ADDR_WIDTH),
    .DATA_WIDTH(DATA_WIDTH)
) dut(
    .clk(clk),
    .reset(reset),

    ///// AXI4-Lite /////
    // Write address
    .axil_awvalid(axil_awvalid),
    .axil_awready(axil_awready),
    .axil_awaddr(axil_awaddr),
    .axil_awprot(axil_awprot),

    // Write data
    .axil_wvalid(axil_wvalid),
    .axil_wready(axil_wready),
    .axil_wdata(axil_wdata),
    .axil_wstrb(axil_wstrb),

    // Write response
    .axil_bvalid(axil_bvalid),
    .axil_bready(axil_bready),
    .axil_bresp(axil_bresp),

    // Read address
    .axil_arvalid(axil_arvalid),
    .axil_arready(axil_arready),
    .axil_araddr(axil_araddr),
    .axil_arprot(axil_arprot),

    // Read data
    .axil_rvalid(axil_rvalid),
    .axil_rready(axil_rready),
    .axil_rdata(axil_rdata),
    .axil_rresp(axil_rresp),

    ///// Wishbone /////
    .wb_adr_o(wb_adr_o),
    .wb_dat_i(wb_dat_i),
    .wb_dat_o(wb_dat_o),
    .wb_we_o(wb_we_o),
    .wb_sel_o(wb_sel_o),
    .wb_stb_o(wb_stb_o),
    .wb_ack_i(wb_ack_i),
    .wb_cyc_o(wb_cyc_o)
);

logic [DATA_WIDTH-1:0] mem [0:2**ADDR_WIDTH-1];
assign wb_dat_i = mem[wb_adr_o];
always @(posedge clk) begin
    if (wb_sel_o && wb_stb_o && wb_cyc_o && wb_ack_i) begin
        if (wb_we_o) begin
            mem[wb_adr_o] <= wb_dat_o;
            bh_info("Wishbone cycle active (write)");
        end else begin
            bh_info("Wishbone cycle active (read)");
        end
    end

    if (axil_awvalid && axil_awready) begin
        bh_info("AXIL write address cycle active");
    end

    if (axil_wvalid && axil_wready) begin
        bh_info("AXIL write data cycle active");
    end

    if (axil_bvalid && axil_bready) begin
        bh_info("AXIL write response cycle active");
    end

    if (axil_arvalid && axil_arready) begin
        bh_info("AXIL read address cycle active");
    end

    if (axil_rvalid && axil_rready) begin
        bh_info("AXIL read data cycle active");
    end
end
integer i;

always #5 clk <= !clk;

initial begin
    $dumpfile("axil_wb_bridge_tb.vcd");
    $dumpvars(0, axil_wb_bridge_tb);

    for (i=0; i<2**ADDR_WIDTH-1; i=i+1) begin
        mem[i] = 0;
    end

    #10
    reset = 0;

    #10
    bh_info("Initialization checks...");
    bh_assert_equal(axil_bvalid, 0, "Initial bvalid");
    bh_assert_equal(axil_rvalid, 0, "Initial rvalid");
    
    // Write 0xdeadbeef @ 12

    #10
    bh_info("Starting write...");
    axil_awaddr = 12;
    axil_awprot = 0; // TODO: check protection value
    axil_awvalid = 1;
    
    #10
    axil_awvalid = 0;

    #10
    axil_wdata = 32'hdeadbeef;
    axil_wstrb = 4'b1111;
    axil_wvalid = 1;

    #10
    axil_wvalid = 0;

    #10
    bh_assert_equal(wb_stb_o, 1, "Wishbone write stb");
    bh_assert_equal(wb_sel_o, 1, "Wishbone write sel");
    bh_assert_equal(wb_cyc_o, 1, "Wishbone write cyc");
    bh_assert_equal(wb_we_o, 1, "Wishbone write we");
    wb_ack_i = 1;

    #10
    bh_assert_equal(wb_stb_o && wb_sel_o && wb_cyc_o, 0, "Wishbone write complete (stb && sel && cyc)");
    wb_ack_i = 0;

    #10
    axil_bready = 1;
    bh_assert_equal(axil_bvalid, 1, "Write bvalid");
    bh_assert_equal(axil_bresp, 0, "Write bresp");

    #10
    bh_assert_equal(axil_bvalid, 0, "Write bvalid");
    axil_bready = 0;
    bh_info("Write complete...");

    // Read 0x00000000 @ 13

    #10
    bh_info("Starting Read...");
    axil_araddr = 13;
    axil_arprot = 0; // TODO: protection values
    axil_arvalid = 1;

    #10
    axil_arvalid = 0;
    
    #10
    bh_assert_equal(wb_stb_o, 1, "Wishbone read stb");
    bh_assert_equal(wb_sel_o, 1, "Wishbone read sel");
    bh_assert_equal(wb_cyc_o, 1, "Wishbone read cyc");
    bh_assert_equal(wb_we_o, 0, "Wishbone read we");
    wb_ack_i = 1;

    #10
    bh_assert_equal(wb_stb_o && wb_sel_o && wb_cyc_o, 0, "Wishbone read complete (stb && sel && cyc)");
    wb_ack_i = 0;

    #10
    bh_assert_equal(axil_rvalid, 1, "Read rvalid");
    bh_assert_equal(axil_rdata, 0, "Read rdata");
    bh_assert_equal(axil_rresp, 0, "Read rresp");
    axil_rready = 1;

    #10
    bh_assert_equal(axil_rvalid, 0, "Read rvalid");
    axil_rready = 0;
    bh_info("Read complete...");

    // Read 0xdeadbeef @ 12

    #10
    bh_info("Starting Read...");
    axil_araddr = 12;
    axil_arprot = 0; // TODO: protection values
    axil_arvalid = 1;

    #10
    axil_arvalid = 0;
    
    #10
    bh_assert_equal(wb_stb_o, 1, "Wishbone read stb");
    bh_assert_equal(wb_sel_o, 1, "Wishbone read sel");
    bh_assert_equal(wb_cyc_o, 1, "Wishbone read cyc");
    bh_assert_equal(wb_we_o, 0, "Wishbone read we");
    wb_ack_i = 1;

    #10
    bh_assert_equal(wb_stb_o && wb_sel_o && wb_cyc_o, 0, "Wishbone read complete (stb && sel && cyc)");
    wb_ack_i = 0;

    #10
    bh_assert_equal(axil_rvalid, 1, "Read rvalid");
    bh_assert_equal(axil_rdata, 32'hdeadbeef, "Read rdata");
    bh_assert_equal(axil_rresp, 0, "Read rresp");
    axil_rready = 1;

    #10
    bh_assert_equal(axil_rvalid, 0, "Read rvalid");
    axil_rready = 0;
    bh_info("Read complete...");

    #10
    bh_assert_stats();
    $finish;
end

endmodule